defmodule DT1 do
  @moduledoc """
    documentation for learning elixir
  """

  @doc """
  let's test all the functions of this language
  """

  def is_it_atom(the_atom) do
    is_atom(the_atom)
  end


  @doc """
    test anonymus functions
  """

  def add_me(a,b) do
    fn a, b -> a + b end
  end



end
