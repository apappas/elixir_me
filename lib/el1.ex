defmodule EL1 do
  @moduledoc """
  Documentation for EL1.
  """

  @doc """
  Hello world.

  ## Examples

      iex> EL1.hello
      :world

  """
  def hello do
    :world
  end

  def sumA(num1, num2) do
    num1 + num2
  end
end
