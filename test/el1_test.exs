defmodule EL1Test do
  use ExUnit.Case
  doctest EL1

  test "greets the world" do
    assert EL1.hello() == :world
  end

  test "sum two numbers" do
    assert EL1.sumA(2, 5) == 7
  end

  test "fail - sum two numbers" do
    assert EL1.sumA(2, 5) != 1
  end

end
