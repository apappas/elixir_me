defmodule DT1Test do
  use ExUnit.Case
  doctest DT1

  test "check if this is an atom" do
    assert DT1.is_it_atom(:koko) == true
  end

  test "this isn't an atom" do
    koko = 7
    IO.puts("lala #{koko}")
    assert DT1.is_it_atom(koko) == false
  end


  test "if this is an anonymus function" do
#    DT1.add_me(3,4) == 7
    is_function(DT1.add_me(1,2))
  end

end
